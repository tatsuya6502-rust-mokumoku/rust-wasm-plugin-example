use std::{ffi::CString, os::raw::c_char};

// The default allocator for wasm32-* target is dlmalloc
// #[global_allocator]
// static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[no_mangle]
pub extern "C" fn name() -> *mut c_char {
    str_to_char_ptr("add")
}

#[no_mangle]
pub extern "C" fn operator() -> *mut c_char {
    str_to_char_ptr("+")
}

#[no_mangle]
pub extern "C" fn calc(a: u32, b: u32) -> u32 {
    a + b
}

/// # Safety
///
/// This function should only be called with *mut c_char pointer
/// previously returned by name() or operator().
#[no_mangle]
pub unsafe extern "C" fn release_string(ptr: *mut c_char) {
    CString::from_raw(ptr);
}

fn str_to_char_ptr(s: &str) -> *mut c_char {
    let s = CString::new(s).expect("Can't create a CString");
    s.into_raw()
}
