/*
 * Complie Command:
 * $ wasm32-wasi-clang -Ofast -nostartfiles -Wl,--no-entry,--export-all -o calc_plugin_mul.wasm calc_plugin_mul.c
 * $ lucetc-wasi -o calc_plugin_mul.so calc_plugin_mul.wasm
 *
 * See also: lucet-wasi-sdk
 * https://github.com/bytecodealliance/lucet/blob/master/lucet-wasi-sdk/src/lib.rs
 * https://github.com/bytecodealliance/lucet/blob/master/lucet-runtime/lucet-runtime-tests/src/build.rs
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// Copied from: https://github.com/bytecodealliance/lucet/blob/master/lucet-runtime/lucet-runtime-tests/guests/entrypoint/use_allocator.c
// The WASI implementation of assert pulls facilities for in printing to stderr
// and aborting. This is lighter weight for a unit test
static void assert(bool v)
{
    if (!v) {
        __builtin_unreachable();
    }
}

char* name()
{
    char str0[] = "mul";
    char *str = (char*) malloc(sizeof(char) * strlen(str0) + 1);
    assert(str);
    strcpy(str, str0);
    return str;
}

char* operator()
{
    char str0[] = "*";
    char *str = (char*) malloc(sizeof(char) * strlen(str0) + 1);
    assert(str);
    strcpy(str, str0);
    return str;
}

uint32_t calc(uint32_t a, uint32_t b)
{
    return a * b;
}

void release_string(char *str)
{
    free(str);
}
