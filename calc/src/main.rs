use calc::PluginManager;
use std::path::PathBuf;

fn main() -> anyhow::Result<()> {
    let mut pm = PluginManager::default();
    pm.load(PathBuf::from("../target/wasm32-wasi/release/calc_plugin_add.so"))?;
    pm.load(PathBuf::from("../target/wasm32-wasi/release/calc_plugin_mul.so"))?;

    for plugin in pm.plugins() {
        println!("Plugin: {}", plugin.name()?);
        println!("Calc: 1 {} 2 = {}", plugin.operator()?, plugin.calc(1, 2)?);
    }

    Ok(())
}
