use anyhow::Result;
use lucet_runtime::{DlModule, InstanceHandle, Limits, MmapRegion, Region};
use std::{ffi::CStr, path::Path};
use wasi_common_lucet::WasiCtxBuilder;

pub struct Plugin {
    instance: InstanceHandle,
}

impl Plugin {
    pub fn name(&mut self) -> anyhow::Result<String> {
        let val = self
            .instance
            .run("name", &[])
            .map_err(anyhow::Error::msg)?
            .unwrap_returned();
        self.transfer_string(val)
    }

    pub fn operator(&mut self) -> anyhow::Result<String> {
        let val = self
            .instance
            .run("operator", &[])
            .map_err(anyhow::Error::msg)?
            .unwrap_returned();
        self.transfer_string(val)
    }

    pub fn calc(&mut self, a: u32, b: u32) -> anyhow::Result<u32> {
        Ok(self
            .instance
            .run("calc", &[a.into(), b.into()])
            .map_err(anyhow::Error::msg)?
            .unwrap_returned()
            .into())
    }

    fn transfer_string(&mut self, val: lucet_runtime::UntypedRetVal) -> anyhow::Result<String> {
        let heap = self.instance.heap();
        let start = val.as_u32() as usize;
        let mut end = start;
        while heap[end] != 0 {
            end += 1
        }
        let c_str = CStr::from_bytes_with_nul(&heap[start..=end])?;
        let s = String::from(c_str.to_string_lossy());
        self.instance
            .run("release_string", &[start.into()])
            .map_err(anyhow::Error::msg)?;
        Ok(s)
    }
}

pub struct PluginManager {
    plugins: Vec<Plugin>,
}

impl Default for PluginManager {
    fn default() -> Self {
        Self::init();
        Self {
            plugins: Vec::default(),
        }
    }
}

impl PluginManager {
    fn init() {
        lucet_runtime::lucet_internal_ensure_linked();
        wasi_common_lucet::export_wasi_funcs();
    }

    pub fn load<P: AsRef<Path>>(&mut self, path: P) -> Result<()> {
        // It seems Rust allocates 16+ pages for the stack.
        // Here, we allow up to 17 pages for the stack and 2 pages for the heap.
        let mem_pages = 17 + 2;
        let limits = Limits {
            heap_memory_size: mem_pages * 64 * 1024,
            ..Limits::default()
        };

        let module = DlModule::load(path).map_err(anyhow::Error::msg)?;
        let region = MmapRegion::create(1, &limits).map_err(anyhow::Error::msg)?;
        let mut instance = region.new_instance(module).map_err(anyhow::Error::msg)?;

        // Add wasi support so that we can see panic! message.
        let wasi_ctx = WasiCtxBuilder::new()
            .and_then(|x| x.inherit_stdio())
            .and_then(|x| x.build())
            .map_err(anyhow::Error::msg)?;
        instance.insert_embed_ctx(wasi_ctx);

        self.plugins.push(Plugin { instance });
        Ok(())
    }

    pub fn plugins(&mut self) -> &mut [Plugin] {
        &mut self.plugins
    }
}
