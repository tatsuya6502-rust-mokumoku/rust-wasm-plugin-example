# Rustのネイティブアプリでwasm形式のプラグインを動的ロードするサンプルプロジェクト

## Requirement

- Linux x86_64 with Docker
- Rust Stable (Tested with 1.40.0)

## Preparation

### Build the Lucet WASM/WASI Toolchain

```console
# Clone the Lucet repository
$ git clone https://github.com/bytecodealliance/lucet.git
$ cd lucet

# Check out a tag
$ git checkout 0.4.1

# Source the setenv script. First run will build Docker images containing the toolchain.
$ source ./devenv_setenv.sh

$ docker images | grep lucet
lucet          latest      9576e750e3de     1 hours ago    3.7GB
lucet-dev      latest      dd5c8b1b4661     1 hours ago    3.51GB
```

### Clone This Example Cargo Workspace

```console
$ pwd
lucet

$ git clone https://gitlab.com/tatsuya6502-rust-mokumoku/rust-wasm-plugin-example.git
```

## Building

```console
$ pwd
lucet

$ source ./devenv_setenv.sh

$ cd rust-wasm-plugin-example
$ pwd
lucet/rust-wasm-plugin-example

# x86_64-unknown-linux-gnu
$ (cd calc && cargo build --release)
$ ls -lh target/release/calc
-rwxr-xr-x. 2 tatsuya tatsuya 7.3M ... target/release/calc

# wasm32-wasi
$ rm ../cargo/config
$ (cd calc-plugin-add && cargo build --release)
$ ls -lh target/wasm32-wasi/release/calc_plugin_add.wasm
-rwxr-xr-x. 2 tatsuya tatsuya 1.8M ... target/wasm32-wasi/release/calc_plugin_add.wasm
$ (cd .. && git restore .cargo/config)

# wasm32-wasi
$ ls -lh target/wasm32-wasi/release/calc_plugin_mul.wasm 
-rwxr-xr-x. 1 root root 16K ... target/wasm32-wasi/release/calc_plugin_mul.wasm
```

```console
$ (cd target/wasm32-wasi/release && \
   lucetc-wasi -o calc_plugin_add.so calc_plugin_add.wasm)

$ (cd target/wasm32-wasi/release && \
   lucetc-wasi -o calc_plugin_mul.so calc_plugin_mul.wasm)

$ ls -lh target/wasm32-wasi/release/*.so  
-rwxr-xr-x. 1 root root 246K ... target/wasm32-wasi/release/calc_plugin_add.so
-rwxr-xr-x. 1 root root  63K ... target/wasm32-wasi/release/calc_plugin_mul.so
```

```console
$ cd ..
$ pwd
lucet

$ ./devenv_stop.sh
Stopping container
lucet
Removing container
lucet
```

## Running

```console
$ pwd
lucet/rust-wasm-plugin-example

$ (cd calc && cargo run --release)
Plugin: add
Calc: 1 + 2 = 3
Plugin: mul
Calc: 1 * 2 = 2
```
